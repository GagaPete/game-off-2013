Splace (Unfinished)
=======
Created for the GitHub Game Off 2013 but not finished in time due to other commitments. Splace is intended to become a space puzzle game.

![Screenshot of splace][screenshot]

[screenshot]: https://gitlab.com/GagaPete/game-off-2013/raw/master/screenshot.png "The screenshot of an early alpha build"
