/**
* World constuctor.
* 
* @class World
* @constructor
*/
function World(width, height) {
    var FLOOR_EXPAND_TICKDELTA = 15;

    var entityList = [];
    
    // Setup floor tile data
    var floorTiles = new Array(width * height);
    for (var i = floorTiles.length - 1; i >= 0; --i) {
        floorTiles[i] = new Tile;
    }
    
    /**
    *
    *
    */
    this.setTileData = function (tileData) {
        if (tileData.length != floorTiles.length) {
            throw new Error("World.setTileData: Length mismatch");
        }

        for (var i = tileData.length - 1; i >= 0; --i) {
            var type;
            switch (tileData[i]) {
                case 0:
                    type = "air";
                    break;
                case 1:
                    type = "oneStep";
                    break;
                case 2:
                    type = "twoStep";
                    break;
                case 10:
                    type = "checkpoint";
                    break;
                case 20:
                    type = "spawn";
                    break;
            }

            floorTiles[i].set(type);
        }
    };
    
    /**
    *
    *
    */
    this.isSolidAt = function (x, y) {
        if (x < 0 || y < 0 || x >= width || y >= width) {
            return false;
        }
        
        return floorTiles[x + (y * width)].solid;
    };

    this.getColorAt = function (x, y) {
        if (x < 0 || y < 0 || x >= width || y >= width) {
            throw new Error("World.getColorAt: Invalid x:y");
        }
        
        return floorTiles[x + y * width].color;
    };

    this.getOpacityAt = function (x, y) {
        if (x < 0 || y < 0 || x >= width || y >= width) {
            throw new Error("World.getOpacityAt: Invalid x:y");
        }
        
        return floorTiles[x + y * width].opacity;
    };

    this.damageFloor = function (x, y) {
        if (x < 0 || y < 0 || x >= width || y >= width) {
            throw new Error("World.damageFloor: Invalid x:y");
        }
        
        floorTiles[x + y * width].damage();
    };
    
    /**
    * Check if the world still has tiles of a given type.
    *
    * @param type string Name of the type to search for.
    */
    this.containsTileType = function (type) {
        for (var i = floorTiles.length - 1; i >= 0; --i) {
            if (floorTiles[i].type == type) {
                return true;
            }
        }

        return false;
    };
    
    /**
    * Gets the (x,y) coordinates of the last orrcuring tile with type of {type}.
    *
    * @param type string Name of the type to search for.
    */
    this.getLastTileWithTypePosition = function (type) {
        for (var i = floorTiles.length - 1; i >= 0; --i) {
            if (floorTiles[i].type == type) {
                return [i % width, (i / width)|0];
            }
        }

        return null;
    };

    /**
    * Change the type of all tiles with a type of {oldType} to {newType}.
    *
    * @param oldType string Name of the type to replace.
    * @param newType strign Name of the type to replace with.
    */
    this.replaceTiles = function (oldType, newType) {
        for (var i = floorTiles.length - 1; i >= 0; --i) {
            if (floorTiles[i].type == oldType) {
                floorTiles[i].set(newType);
            }
        }
    };
    
    /**
    * Create an entity in the world.
    * 
    * @param EntityType Constructor of the wanted entity.
    * @param x X-position of the entity.
    * @param y Y-position of the entity.
    */
    this.spawn = function (EntityType, x, y, z) {
        z = z ? z : 0;
        var entity = new EntityType(this, x, y, z);
        
        entityList.push(entity);
        
        return entity;
    };
    
    /**
    * Remove an entity from the world.
    * 
    * @param entity An entity instance
    */
    this.despawn = function (entity) {
        entities.splice(entities.indexOf(entity), 1);
    };
    
    /**
    *
    *
    *
    */
    this.tick = function () {
        for (var i = floorTiles.length - 1; i >= 0; --i) {
            floorTiles[i].tick();
        }

        for (var i = entityList.length - 1; i >= 0; --i) {
            entityList[i].tick();
        }
    };
};
