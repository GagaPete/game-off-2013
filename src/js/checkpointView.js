function CheckpointView(boardView) {
    var rotation = 0;
    
    this.render = function (context) {
        rotation += 1;
        
        var size = Math.sin(Math.PI / 4) * 0.3;
        
        var x1 = Math.sin(Math.PI * (rotation % 90) / 180) * size;
        var x2 = Math.sin(Math.PI * (90 + (rotation % 90)) / 180) * size;
        var x3 = Math.sin(Math.PI * (180 + (rotation % 90)) / 180) * size;
        var x4 = Math.sin(Math.PI * (270 + (rotation % 90)) / 180) * size;
        var y1 = Math.cos(Math.PI * (rotation % 90) / 180) * size;
        var y2 = Math.cos(Math.PI * (90 + (rotation % 90)) / 180) * size;
        var y3 = Math.cos(Math.PI * (180 + (rotation % 90)) / 180) * size;
        var y4 = Math.cos(Math.PI * (270 + (rotation % 90)) / 180) * size;
        
        var p0 = boardView.tilePixelCenter(5.5, 5.5, 0.3);
        var p1 = boardView.tilePixelCenter(5.5 + x1, 5.5 + y1);
        var p2 = boardView.tilePixelCenter(5.5 + x2, 5.5 + y2);
        var p3 = boardView.tilePixelCenter(5.5 + x3, 5.5 + y3);
        var p4 = boardView.tilePixelCenter(5.5 + x4, 5.5 + y4);
        
        // draw wall 1
        context.fillStyle = "rgba(0, 180, 0, 0.8)";
        context.beginPath();
        context.moveTo(p0[0], p0[1]);
        context.lineTo(p2[0], p2[1]);
        context.lineTo(p3[0], p3[1]);
        context.closePath();
        context.fill();
        context.beginPath();
        context.moveTo(p0[0], p0[1]);
        context.lineTo(p3[0], p3[1]);
        context.lineTo(p4[0], p4[1]);
        context.closePath();
        context.fill();
        context.beginPath();
        context.moveTo(p0[0], p0[1]);
        context.lineTo(p4[0], p4[1]);
        context.lineTo(p1[0], p1[1]);
        context.closePath();
        context.fill();
        context.beginPath();
        context.moveTo(p0[0], p0[1]);
        context.lineTo(p1[0], p1[1]);
        context.lineTo(p2[0], p2[1]);
        context.closePath();
        context.fill();
        
        //context.fillRect(60+x2, 30, x1 - x2, 30);
        //context.fillStyle = "#ddd";
        //context.fillRect(60+x3, 30, x2 - x3, 30);
    };
};
