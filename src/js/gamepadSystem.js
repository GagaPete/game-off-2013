(function () {
    var idRegexpr = new RegExp("Vendor: (\\d+) Product: (\\d+)");
    
    function getMapping(gamepad) {
        var result = idRegexpr.exec(gamepad.id);
        var vendor = result[1];
        var product = result[2];
        var mapping;
        
        for (i in settings.gamepad.mappings) {
            mapping = settings.gamepad.mappings[i];
            if (mapping.vendor == vendor && mapping.product == product) {
                return mapping;
            }
        }
        
        return null;
    }
    
    function ChromeGamepadSystem (splace) {
        var gamepadSystem = this;
        
        var active = null;
        
        this.update = function () {
            var gamepads = navigator.webkitGetGamepads();
            var gamepad;
            var mapping;
            var stayActive = false;
            
            for (var i = gamepads.length; i >= 0; --i) {
                gamepad = gamepads[i];
                
                if (gamepad) {
                    // If no gamepad is active, try to activate
                    if (active == null) {
                        mapping = getMapping(gamepad);
                        if (mapping) {
                            active = [gamepad, mapping.mapping];
                            stayActive = true;
                        }
                    } else if (active[0].index == gamepad.index) {
                        stayActive = true;
                    }
                }
            }
            
            if (!stayActive) {
                active = null;
            }
            
            if (active) {
                var gamepad = active[0];
                var mapping = active[1];
                var map;
                var value;
                
                for (i in mapping) {
                    map = mapping[i];
                    if (map.type == "axis") {
                        value = gamepad.axes[map.id];
                        if (map.low) {
                            gamepadSystem[map.low] = (value < -0.8);
                        }
                        if (map.high) {
                            gamepadSystem[map.high] = (value > 0.8);
                        }
                    }
                };
            }
        };
    };
    
    if ('webkitGetGamepads' in navigator) {
        window.GamepadSystem = ChromeGamepadSystem;
    }
} ());
