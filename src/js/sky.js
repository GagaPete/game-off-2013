function Sky () {
    this.render = function (context, width, height) {
        context.fillStyle = "white";
        for (var x = 0, y; x < width; ++x) {
            for (y = 0; y < height; ++y) {
                if (Math.random() < 0.0005) {
                    var size = (Math.random() * 1.5 + (Math.random() > 0.8 ? 0.5 : 0));
                    context.fillRect(x - (size / 2), y - (size / 2), size, size);
                }
            }
        }
    };
}
