function GameStage (splace) {
    // Game progress
    var currentLevelTicks = 0;
    var currentLevel = 0;
    var currentStepCount = 0;
    var worldSize = 15;
    var world = new World(worldSize, worldSize);
    var player = world.spawn(Player, 7, 7);
    
    // Dialog processing
    // 
    var DIALOG_FADEIN = 30;
    var DIALOG_FADEOUT = 30;
    
    var dialogQueue = new Array();
    var dialogVisibleSince = null;
    var dialogOpacity = null;
    var dialogText = null;
    // TO IMPLEMENT
    
    // Game progress management
    //
    //
    var AUTOSAVE_INTERVAL = 360;
    var timeUntilAutosave = AUTOSAVE_INTERVAL;

    function restoreProgress() {
        var progress_version = localStorage.getItem("progress_version");
        if (progress_version != splace.version) {
            // version of savegame don't match games version, consider invalid
            return;
        }

        var progress = JSON.parse(localStorage.getItem("progress_data"));
        currentLevel = progress.currentLevel;
        currentStepCount = progress.currentStepCount;
    };

    function saveProgress() {
        var progress = {
            currentLevel: currentLevel,
            currentStepCount: currentStepCount
        };
        localStorage.setItem("progress_data", JSON.stringify(progress));
        localStorage.setItem("progress_version", splace.version);
    };
    
    function setupGame() {
        currentLevel = 0;
        world.setTileData(LevelTree[0].data);
        console.log(world.getLastTileWithTypePosition("spawn"));
    }
    setupGame();
    
    function nextLevel() {
        currentLevel++;
        if (currentLevel in LevelTree) {
            world.setTileData(LevelTree[currentLevel].data);
        } else {
            world.setTileData(new Array(worldSize * worldSize));
        }
    };
    
    /**
    *
    *
    */
    this.enter = function () {
        restoreProgress();
        setupGame();
    };
    
    /**
    * Leave the game screen.
    *
    */
    this.leave = function () {
        saveProgress();
    };
    
    /**
    *
    *
    */
    this.tick = function (keyboard) {
        // Autosave in interval
        if ((timeUntilAutosave--) == 0) {
            saveProgress();
            timeUntilAutosave = AUTOSAVE_INTERVAL;
        }
        
        world.tick();
        
        if (!world.containsTileType("checkpoint")) {
            // if no checkpoints remains
            world.replaceTiles("oneStep", "air");
            world.replaceTiles("twoStep", "air");
            world.replaceTiles("spawn", "air");
            world.replaceTiles("checkpointChecked", "spawn");

            nextLevel();
        }
    };

    /**
    * Render dialog text to the screen.
    *
    */
    function renderDialog(context, x, y, width, height, text) {
        var size = width / 60;
        context.font = size + "px 'Roboto', sans-serif";
        
        var textWidth = context.measureText(text);
        context.fillStyle = "#000";
        context.fillRect(x + (width + textWidth.width + 20) / 2, y + (size + height) / 2, textWidth.width + 20, size + 20);
        context.textBaseline = "baseline";
        context.fillStyle = "#fff";
        context.fillText(text, x + 10 + (width - textWidth.width) / 2, y + 10 + (size + height) / 2);
    }
    
    /**
    * Render the world.
    *
    */
    function renderWorld(context, x, y, width, height) {
        var ratio = Math.sin(Math.PI / 4) * 2;
        var size = Math.min(width, height * 2) / worldSize / ratio;
        
        var tileWidth = size * ratio, tileWidthHalf = tileWidth / 2;
        var tileHeight = tileWidth / 2, tileHeightHalf = tileHeight / 2;;

        var totalWidth = tileWidth * 16;
        var totalHeight = tileHeight * 16;

        var left = x + ((width - (tileWidth * 16)) / 2) + tileWidth * 8;
        var top = y + (height - (tileHeight * 17)) / 2 + size;

        // Render the grid
        var color;
        var opacity;

        for (var x = 0; x < worldSize; ++x) {
            for (var y = 0; y < worldSize; ++y) {
                color = world.getColorAt(x, y);
                opacity = world.getOpacityAt(x, y);
                context.fillStyle = "rgba("+color[0]+"," + color[1] + "," + color[2] + ","+opacity + ")";
                context.beginPath();
                context.moveTo(left + (x - y) * tileWidthHalf, top + (x + y) * tileHeightHalf);
                context.lineTo(left + (x - y + 1) * tileWidthHalf, top + (x + y + 1) * tileHeightHalf);
                context.lineTo(left + (x - y) * tileWidthHalf, top + (x + y + 2) * tileHeightHalf);
                context.lineTo(left + (x - y - 1) * tileWidthHalf, top + (x + y + 1) * tileHeightHalf);
                context.closePath();
                context.fill();
            }
        }
        
        // Render the player
        x=player.x;y=player.y,z=player.z;
        top += player.z * size;
        context.fillStyle = "#fff";
        left += 0.5;
        context.beginPath();
        context.moveTo(left + (x - y) * tileWidthHalf, top + (x + y + 2) * tileHeightHalf - size);
        context.lineTo(left + (x - y - 1) * tileWidthHalf, top + (x + y + 1) * tileHeightHalf - size);
        context.lineTo(left + (x - y - 1) * tileWidthHalf, top + (x + y + 1) * tileHeightHalf);
        context.lineTo(left + (x - y) * tileWidthHalf, top + (x + y + 2) * tileHeightHalf);
        context.closePath();
        context.fill();
        context.fillStyle = "#ddd";
        left -= 0.5;
        context.beginPath();
        context.moveTo(left + (x - y) * tileWidthHalf, top + (x + y + 2) * tileHeightHalf - size);
        context.lineTo(left + (x - y + 1) * tileWidthHalf, top + (x + y + 1) * tileHeightHalf - size);
        context.lineTo(left + (x - y + 1) * tileWidthHalf, top + (x + y + 1) * tileHeightHalf);
        context.lineTo(left + (x - y) * tileWidthHalf, top + (x + y + 2) * tileHeightHalf);
        context.closePath();
        context.fill();
        context.fillStyle = "#eee";
        top += 0.5;
        context.beginPath();
        context.moveTo(left + (x - y) * tileWidthHalf, top + (x + y) * tileHeightHalf - size);
        context.lineTo(left + (x - y + 1) * tileWidthHalf, top + (x + y + 1) * tileHeightHalf - size);
        context.lineTo(left + (x - y) * tileWidthHalf, top + (x + y + 2) * tileHeightHalf - size);
        context.lineTo(left + (x - y - 1) * tileWidthHalf, top + (x + y + 1) * tileHeightHalf - size);
        context.closePath();
        context.fill();
    }

    /**
    * Render the games current state.
    *
    */
    this.render = function (context, width, height) {
        var dialog = "Welcome to the space, A.I. #2348.2c";
        //var dialog = "Welcome, to the experiments in Splace.";
        //var dialog = "Start moving by using your arrow keys, your gamepad or your touchscreen.";
        //var dialog = "Let's start simple, I don't want to stress you.";
        //var dialog = "I just want to inform you, that all your actions will be filed.";
        
        renderDialog(context, 0, height * 5 / 6, width, height / 6, dialog);
        renderWorld(context, 0, height * 1 / 6, width, height * 4 / 6);
    };


    /**
    *
    *
    */
    this.pushDialog = function (message, lifetime) {
        lifetime = lifetime || 1000;
        
        dialogQueue.push([message, lifetime])
    }
}
