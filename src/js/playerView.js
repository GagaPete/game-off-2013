function PlayerView(boardView, player) {
    this.render = function (context) {
        var p = boardView.tilePixelCenter(player.x, player.y);
        var s = boardView.getTileSize();
        
        context.fillStyle = "#ddd";
        context.beginPath();
        context.moveTo(p[0], p[1] + (s[2] / 2) - s[0]);
        context.lineTo(p[0], p[1] + (s[2] / 2));
        context.lineTo(p[0] + (s[1] / 2), p[1] + s[2]);
        context.lineTo(p[0] + (s[1] / 2), p[1] + s[2] - s[0]);
        context.fill();
        
        context.fillStyle = "#aaa";
        context.beginPath();
        context.moveTo(p[0] + s[1], p[1] + (s[2] / 2) - s[0]);
        context.lineTo(p[0] + s[1], p[1] + (s[2] / 2));
        context.lineTo(p[0] + (s[1] / 2), p[1] + s[2]);
        context.lineTo(p[0] + (s[1] / 2), p[1] + s[2] - s[0]);
        context.fill();

        p[1] += 0.1;
        context.fillStyle = "#fff";
        context.beginPath();
        context.moveTo(p[0] + s[1], p[1] + (s[2] / 2) - s[0]);
        context.lineTo(p[0] + (s[1] / 2), p[1] + s[2] - s[0]);
        context.lineTo(p[0], p[1] + (s[2] / 2) - s[0]);
        context.lineTo(p[0] + (s[1] / 2), p[1] - s[0]);
        context.fill();
    };
}
