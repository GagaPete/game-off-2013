function Checkpoint(world, x, y) {
    var capturedBy = null;
    
    this.tick = function () {
        var players = world.getPlayers();
        var player;
        
        for (var i = players.length; i >= 0; --i) {
            player = players[i];
            
            if (player.x == x && player.y == y) {
                capturedBy = player;
            }
        }
    };
};
