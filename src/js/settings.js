var settings = {
    gamepad: {
        mappings: [
            {
                vendor: "0079",
                product: "0011",
                sytem: "Lin",
                mapping: [
                    {
                        type: "axis",
                        id: 0,
                        low: "dpadLeft",
                        high: "dpadRight"
                    },
                    {
                        type: "axis",
                        id: 1,
                        low: "dpadUp",
                        high: "dpadDown"
                    }
                ]
            }
        ]
    }
};
