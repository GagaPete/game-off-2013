function Keyboard () {
    var keyBuffer = {};
    
    this.update = function () {
        
    };
    
    this.isDown = function (keyCode) {
        return !!keyBuffer[keyCode];
    };
    
    function handleKeyDown(e) {
        keyBuffer[e.keyCode] = true;
    };
    
    function handleKeyUp(e) {
        keyBuffer[e.keyCode] = false;
    };
    
    function resetKeyBuffer() {
        keyBuffer = {};
    };
    
    // listen key events
    window.addEventListener("keydown", handleKeyDown, false);
    window.addEventListener("keyup", handleKeyUp, false);
    
    this.destroy = function () {
        // unlisten key events
        window.removeEventListener("keydown", handleKeyDown, false);
        window.removeEventListener("keyup", handleKeyUp, false);
    };
}

window.keyboard = new Keyboard;
