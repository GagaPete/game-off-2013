(function (window, document) {
    function checkRequirements() {
        if (!("JSON" in window)) {
            alert ("Your browser don't support JSON.");
            throw new Error("Missing JSON support");
        }
        if (!("requestAnimationFrame" in window)) {
            alert ("Your browser don't support requestAnimationFrame.");
            throw new Error("Missing requestAnimationFrame support");
        }
    };
    
    function Splace(options) {
        /** Assign instance to reference in callbacks */
        var splace = this;
        
        checkRequirements();
        
        /** Game name */
        this.name = "Splace";
        
        /** Game version */
        this.version = "alpha3";
        
        if (!("target" in options)) {
            throw "Splace(): Missing 'target' option";
        }
        var canvas = options.target;
        var context = canvas.getContext("2d");
        
        var keyboard = new Keyboard;
        var gamepads = new GamepadSystem;
        gamepads.update();
        
        var isPaused = false;
        
        var tickTime = (new Date).getTime();
        var ticksPerSecond = 60;
        var tickDelta = 1000 / ticksPerSecond;
        
        var stages = {
            menu: new MenuStage(splace),
            game: new GameStage(splace),
            result: new ResultStage(splace)
        };
        var stage = stages.game;
        stage.enter();
        
        var actions = {
            moveNorth: false,
            moveEast: false,
            moveSouth: false,
            moveWest: false,
            anyButton: false
        };
        
        // Prepare the sky result cache
        var skyCache;
        function prepareSkyCache() {
            var sky = new Sky;
        
            skyCache = document.createElement("canvas");
            skyCache.width = canvas.width;
            skyCache.height = canvas.height;
            skyCache.getContext("2d").fillRect(0, 0, skyCache.width, skyCache.height);
            sky.render(skyCache.getContext("2d"), skyCache.width, skyCache.height);
        }
        prepareSkyCache();
        
        
        /**
        * Tick function will be called 120 times per second.
        * 
        * Use this to move objects in space and to check lifetimes and alike.
        *
        */
        function tick() {
            if ('tick' in stage) {
                stage.tick(keyboard) // TODO Not pass keyboard as param
            }
        };
        
        
        /**
        * Render backgrounds, Debug behaviors and the current stage. 
        * 
        */
        function render() {

            context.save();
            context.drawImage(skyCache, 0, 0);
            if ('render' in stage) {
                stage.render(context, canvas.width, canvas.height)
            }
            context.restore();
            
            // Render version text
            context.fillStyle = "white";
            context.fillText(splace.name + " ver. " + splace.version, 0, 10);
        };
        
        
        /**
        * The game loop.
        * 
        * Queues itself for the next render frame using requestAnimationFrame.
        */
        function mainLoop() {
            // Update keyboard and gamepads
            keyboard.update();
            gamepads.update();
            
            g = gamepads;
            
            var currentTime = (new Date).getTime();
            while ((tickTime + tickDelta) <= currentTime) {
                tick();
                
                tickTime += tickDelta;
            };
            
            render();
            
            // queue the main loop for the next frame if the game is not paused
            if (!isPaused) {
                requestAnimationFrame(mainLoop);
            }
        }
        
        
        /**
        * Pause the game if it is running.
        * 
        */
        function pause() {
            if (!isPaused) {
                // add paused flag
                isPaused = true;
                
                // stop the main loop
                cancelAnimationFrame(mainLoop);
            }
        }
        
        
        /**
        * Resume the game after it has been paused. 
        * 
        */
        function resume() {
            if (isPaused) {
                // remove paused flag
                isPaused = false;
                
                // to avoid jumps in the simulation
                tickTime = (new Date).getTime();
                
                // resume the main loop
                requestAnimationFrame(mainLoop);
            }
        }
        
        /**
        * Prepare the game to be quit.
        *
        */
        function stop() {
            // Notify current stage that it is going to be leaved
            if (stage !== null) {
                stage.leave();
                stage = null;
            }
            
            isPaused = true;
            
            // Remove listeners to avoid duplicate unloading
            window.removeEventListener("unload", stop, false);
            window.removeEventListener("beforeunload", stop, false);
        };
        // Add listeners for known unload events
        window.addEventListener("unload", stop, false);
        window.addEventListener("beforeunload", stop, false);
        
        // Disable context menu for the sake of real game feeling
        window.addEventListener("contextmenu", function(event) {
            event.preventDefault();
        }, false);
        
        // Start the main loop
        mainLoop();
    };
    
    window.Splace = Splace;
} (window, document));
