function loadLevel(url, callback) {
    var xhr = new XMLHttpRequest;
    xhr.open("GET", url);
    xhr.onload = function () {
        var result = JSON.parse(xhr.responseText);
        if (typeof result.data != "object" || typeof result.data.pop != "function") {
            throw new Error("Level result is not a array");
        }
        if (typeof result.width != "number") {
            throw new Error("Level width is not a number");
        }
        if (typeof result.height != "number") {
            throw new Error("Level height is not a number");
        }
        
        callback(result);
    };
    try {
        xhr.send();
    } catch (e) {
    }
};
