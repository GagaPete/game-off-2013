/**
* Player entity.
* 
* @class Player
* @constructor
*/
function Player (world, x, y) {
    this.world = world;
    this.x = x;
    this.y = y;
    this.z = 0;

    var oldTileX = x;
    var oldTileY = y;
    
    var direction = -1;
    var remaining = 0;

    var falling = false;
    var fallSpeed = 1 / 5;
    
    var speed = 1 / 20;
    
    function choseDirection() {
        if (keyboard.isDown(37)) {
            direction = 0;
        };
        if (keyboard.isDown(39)) {
            direction = 2;
        };
        if (keyboard.isDown(38)) {
            direction = 3;
        };
        if (keyboard.isDown(40)) {
            direction = 1;
        };
        if (direction != -1) {
            remaining = 20;
        };
    };
    
    this.tick = function (keyboard) {
        if (falling) {
            this.z += fallSpeed;
        }
        if (remaining > 0) {
            if (remaining == 10) {
                world.damageFloor(oldTileX, oldTileY);
            }
            switch (direction) {
                case 0:
                    x -= speed;
                    break;
                case 1:
                    y += speed;
                    break;
                case 2:
                    x += speed;
                    break;
                case 3:
                    y -= speed;
                    break;
            }
            remaining--;
        } else {
            x = Math.round(x);
            y = Math.round(y);

            direction = -1;
            
            choseDirection(keyboard);

            if (!world.isSolidAt(x, y)) {
                falling = true;
            }

            oldTileX = x;
            oldTileY = y;
        }
        this.x = x;
        this.y = y;
    };
};
